# save history of commands
set history save on
set history expansion on

# beautifiers
set print pretty on
set print object on
set print static-members on
set print vtbl on
set print demangle on
set demangle-style gnu-v3
set print sevenbit-strings off

set follow-fork-mode child
set detach-on-fork off

set confirm off
set verbose off
set prompt \033[31mgdb$ \033[0m

set output-radix 0x10
set input-radix 0x10

# These make gdb never pause in its output
set height 0
set width 0

# Allow local customizations in the .gdbinit_local file
source ~/.gdbinit_local
