# Use colors in coreutils utilities output
# ls options: A = include hidden (but not . or ..), F = put `/` after folders
if [ $(uname -s) != "Darwin" ]; then
    # macos color options are not the same
    alias ls='ls --color=auto'
    alias grep='grep --color'
    alias l.='ls -d .* --color=auto'
    alias ll='ls -lF --color=auto'
fi

# navigation
alias ..='cd ..'
alias ...='cd ../..'

# git related
alias gl='git log --oneline --decorate'
alias gg='git log --graph --full-history --all --color --pretty=format:"%x1b[31m%h%x09%x1b[32m%d%x1b[0m%x20%s"'

# remappings - old school
alias pico='nano -w'

# tmux related
alias ta='tmux -u -CC attach'
alias tn='tmux -u -CC'
