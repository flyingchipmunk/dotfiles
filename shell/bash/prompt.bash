if [ -f /etc/bash_completion.d/git ]; then
	source /etc/bash_completion.d/git

	# Configure `__git_ps1` to tell us as much as possible
	export GIT_PS1_SHOWDIRTYSTATE=1 GIT_PS1_SHOWSTASHSTATE=1 GIT_PS1_SHOWUNTRACKEDFILES=1
	export GIT_PS1_SHOWUPSTREAM=verbose GIT_PS1_DESCRIBE_STYLE=branch GIT_PS1_SHOWCOLORHINTS=1
	export GIT_PS1_HIDE_IF_PWD_IGNORED=1

	# Colorful prompt for Bash!
	export PS1='\[\e[0;36m\][\A] \u@\h:\[\e[0m\e[0;32m\]\W\[\e[1;33m\]$(__git_ps1 " (%s)")\[\e[0;37m\] \$\[\e[0m\] '

	# Or more compact
	#export PS1='[\u@\h`__git_ps1` \W]\$ '
fi

