# TODO: invest in https://github.com/Bash-it/bash-it

HISTSIZE=10000
HISTFILE="$HOME/.bash_history"
SAVEHIST=$HISTSIZE

# append to history file
shopt -s histappend

# Don't save duplicates
export HISTCONTROL=ignoredups

# Autocorrect typos in path names when using `cd`
shopt -s cdspell

# Check the window size after each command and, if necessary, update the values
# of LINES and COLUMNS.
shopt -s checkwinsize

# Case-insensitive globbing (used in pathname expansion)
#shopt -s nocaseglob
