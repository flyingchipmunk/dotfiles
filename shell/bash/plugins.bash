# dircolors
if [[ "$(tty -s && tput colors)" == "256" ]]; then
    cmd=""
    if command -v dircolors >/dev/null 2>&1; then
        cmd="dircolors"
    elif command -v gdircolors >/dev/null 2>&1; then
        cmd="gdircolors"
    fi
    if [ ! -z "$cmd" ]; then
        eval "$(${cmd} ~/.shell/plugins/dircolors-solarized/dircolors.256dark)"
    fi
fi
