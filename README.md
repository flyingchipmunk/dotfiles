# Matt's Dotfiles

This repo aims to aid in the installation, configuration and synchronization of "[dotfiles](https://dotfiles.github.io/)".

After cloning this repo, run ```./install``` to set up the base environment. Note that the install script is idempotent: it can safely be run multiple times.

For computer-specific configuration, use ```dotfiles-local``` to make customizations for specific computers or groups of computers.

Based on [Anish Athalye's](http://www.anishathalye.com/2014/08/03/managing-your-dotfiles/) [dotfiles](https://github.com/anishathalye/dotfiles). Dotfiles uses [Dotbot](https://git.io/dotbot) for installation.

&nbsp;

## Big Picture Layout

It's important to note that there is minimal magic happening here. The idea is simple: symlink all your `~/.*` files into a git repo making it easy to setup, update and synchronize to all your computers. The approach here is trying to address two concepts: 1) Public vs Private configuration and 2) Differences between specific computers (or groups of computers).

### Public vs Private configuration

There may be some configuration that is either so specific to you that it would not benefit others, or contain personally identifiable information such as usernames or email address. To address this we split the configuration into two repos:

`dotfiles` - Contains non-personally identifiable platform independent configuration.

`dotfiles-local` - Contains any personally identifiable or otherwise private configuration in a **private** repo.

### Differences between specific computers (or groups of computers)

One possible approach to handle different configurations for computers is to use different branches in `dotfiles-local` to represent specific computers (or groups of computers). A possible branching strategy could be:

```
master              -> Private platform independent configuration
|
+- linux-desktop    -> Branch for specific configurations for a linux-desktop
|
+- linux-server     -> Branch for specific configurations for a linux-server
|
+- mac              -> Branch for specific configurations for a mac
```

&nbsp;

## Dotfiles Layout

Start from `install.conf.yaml` and make any changes to suit your needs. All files are optional, this is just a possible layout option.

```
install.conf.yaml      -> Install configuration, start here
|
+- gdbinit             -> General platform independent gdb config
|
+- gitconfig           -> General platform independent git config
|
+- gitignore_global    -> All platform inclusive gitignore for all repos
|
+- shell/              -> Everything shell related
|  |
|  +- aliases.sh       -> Platform independent shell aliases
|  |
|  +- exports.sh       -> Platform independent shell exports
|  |
|  +- bash/            -> Directory per shell (functions, prompt, settings)
|  |
|  +- plugins/         -> Any git submodules for shell
|
+- tmux.conf           -> General tmux config
```

&nbsp;

## License

Copyright (c) 2017 Matthew Veno. Released under the MIT License. See
[LICENSE](LICENSE) for details.