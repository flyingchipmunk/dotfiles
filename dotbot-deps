#!/usr/bin/env bash

##
## A simple script to attempt to auto-install the base dependency of dotbot
##

set +e

dep_name="python-argparse"

function trap_error() {
  echo "ERROR: Unable to install '${dep_name}' automatically, please install manually."
  exit 1
}

/usr/bin/env python -c "import argparse" > /dev/null 2>&1
if [ $? -ne 0 ]; then
  trap trap_error ERR
  prefix=''
  if (( EUID != 0 )); then
    echo "INFO: must be root to install dependency, attempting to use sudo"
    prefix='sudo'
  fi
  if [ -x "/usr/bin/yum" ]; then
    ${prefix} /usr/bin/yum -q -y --enablerepo='*' install ${dep_name}
  elif [ -x "/usr/bin/apt-get" ]; then
    ${prefix} /usr/bin/apt-get -q -y install ${dep_name}
  else
    echo "ERROR: Unable to determine package manager to install '${dep_name}' automatically, please install manually."
    exit 1
  fi
fi

exit 0
